#coding: utf8
import cherrypy
import sys
import os
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
 
sys.path.append(_curdir)
 
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    inOpenshift = True
else:
    inOpenshift = False
 
if inOpenshift:
    # while program is executed in OpenShift
    download_root_dir = os.environ['OPENSHIFT_DATA_DIR']
    data_dir = os.environ['OPENSHIFT_DATA_DIR']
else:
    # while program is executed in localhost
    download_root_dir = _curdir
    data_dir = _curdir
#從 1 累加到 50, 總數是多少? 請將程式寫在 OpenShift 平台上, 採用 URL 輸入變數的模式編寫
class Root(object):
    """docstring
    從 1 累加到 50, 總數是多少? 請將程式寫在 OpenShift 平台上, 採用 URL 輸入變數的模式編寫
    """
    # 表單有中文要加上這個
    _cp_config = {
        # if there is no utf-8 encoding, no Chinese input available
        'tools.encode.encoding': 'utf-8',
        'tools.sessions.on' : True,
        'tools.sessions.storage_type' : 'file',
        'tools.sessions.locking' : 'explicit',
        'tools.sessions.storage_path' : data_dir+'/tmp',
        # session timeout is 60 minutes
        'tools.sessions.timeout' : 60
    }
    def index(self):
        '''
        這裡是首頁喔!
        '''
        Outstring = '''
        <h1>從 start 累加到 end, 總數是多少</h1>
        <h2>假如任意一值為空或不為數字則會導回此頁面,大小填反系統會自動切換</h2>
        <form method="post" action="doAct">
        起始點:<input type=text name=start value=1 ><br />
        結束點:<input type=text name=end value=1 ><br />
        <input type="submit" value="send">
        <input type="reset" value="reset">
        </form>
        '''
        return Outstring
    index.exposed = True
 
    def doAct(self, start=None, end=None):
        total = 0
        if(start and end):
            try:
                start = int(start)
                end = int(end)
                #轉換失敗 可能是 使用者輸入值不能轉為數字 int 輸入 不為數字 如 : a, a123
            except:
                #轉換失敗 導向首頁
                raise cherrypy.HTTPRedirect("/")
                #return "some input is not correct<br />" + "<a href=\"/index\">Index</a>"
            if start>end:
                start, end = end, start
            for i in range(start, end + 1 ):
                total = total + i
            return "總和:" + str(total) + "<a href=\"/\">Index</a>"
        #假如沒有 start or end  導向首頁
        raise cherrypy.HTTPRedirect("/")
    doAct.exposed = True
 
root = Root()
 
# setup static, images and downloads directories
application_conf = {
    '/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"
    },
    '/images':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': data_dir+"/images"
    },
    '/downloads':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': data_dir+"/downloads"
    }
}
 
#不同的環境有不同的啟動方式
# if inOpenshift ('OPENSHIFT_REPO_DIR' exists in environment variables) or not inOpenshift
if inOpenshift:
    # operate in OpenShift
    application = cherrypy.Application(root, config = application_conf)
else:
    # operate in localhost
    cherrypy.quickstart(root, config = application_conf)